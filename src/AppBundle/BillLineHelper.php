<?php

namespace App\AppBundle;


use App\Entity\BillLine;
use App\Entity\Bill;

/**
 * Classe facilitant la création des lignes de facturation
 */
class BillLineHelper {


    // Fonction permettant le création rapide d'une ligne de facturation
    static public function lineHelper( Bill $bill ,$productOrService, $quantity, $unitPrice, $totalPrice)
    {
           $billLine = new BillLine();
           
           $billLine->setBill($bill)
                    ->setDescription($productOrService)
                    ->setQuantity($quantity)
                    ->setUnitPrice($unitPrice)
                    ->settotalPrice($totalPrice);

                    return $billLine;
    }

}