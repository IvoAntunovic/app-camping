<?php

namespace App\Repository;

use App\Entity\BillCustomerInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BillCustomerInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method BillCustomerInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method BillCustomerInfo[]    findAll()
 * @method BillCustomerInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BillCustomerInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BillCustomerInfo::class);
    }

    // /**
    //  * @return BillCustomerInfo[] Returns an array of BillCustomerInfo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BillCustomerInfo
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
