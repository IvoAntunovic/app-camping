<?php

namespace App\Form;


use App\Entity\Booking;
use Doctrine\DBAL\Types\TextType;
use PharIo\Manifest\Email;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Form\BookingType;

class FormBookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('startDate', DateType::class, [
            'placeholder' => 'Select a value'
        ])
        ->add('sleepingAdult')
        ->add('sleepingChild')
        ->add('swimingPoolOption', ChoiceType::class, [
                     'choices' => [
                         'Sans' => 2,
                         'Avec' => 1
                     ]
        ])
        ->add('swimingPoolAdult', IntegerType::class, [
            'label' => 'Nombre de place adult pour la piscine '
        ])
        ->add('swimingPoolChild', IntegerType::class, [
            'label' => 'Nombre de place enfant pour la piscine '
        ])


        ->add('endDate', DateType::class, [
            'placeholder' => 'Select a value'
        ])
        ->add('firstName')
        ->add('name')
        ->add('email')
        ->add('phonNumber')
        ->add('city')
        ->add('street')
        ->add('submit', SubmitType::class, ['label' => 'Enregister la réservation'])
    ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
