<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class BackOfficeController
 * @package App\Controller
 * @Route("/backoffice")
 */
class BackOfficeController extends AbstractController
{
    


    public function __construct()
    {

    }





    /**
     * @Route("backoffice", name="backoffice")
     */
    public function admin()
    {
          

        return $this->render("backoffice/backoffice.html.twig");
    }

}