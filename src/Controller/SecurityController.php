<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package App\Controller
 * @Route("/security")
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request){

        $manager = $this->getDoctrine()->getManager();
    
        $user = new User();

        $form = $this->createform(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){


            $manager->persist($user);

            $manager->flush();

            $role = '';

             if($user->getRoles()[0] == 'ROLE_OWNER'){
                 $role = 'ROLE_OWNER';
             }
             if($user->getRoles()[0] == 'ROLE_ADMIN'){
                 $role = 'ROLE_ADMIN';
             }

            return $this->redirectToRoute('security_login', [
                'role' => $role
            ]);
           
        }

        return $this->render("security/registration.html.twig", [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils){

        $error = $authenticationUtils->getLastAuthenticationError();
        
    
    

        return $this->render("security/login.html.twig", [
            'error' => $error
        ]);
    }

 
    /**
     * @Route("/backoffice", name="backoffice")
     */
    public function backoffice()
    {
        return $this->render("security/backoffice.html.twig");
    }
}
