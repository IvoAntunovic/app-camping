<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;



/**
 * Class OwnerController
 * @package App\Controller
 * @Route("/owner")
 */
class OwnerController extends AbstractController
{


    public function __construct()
    {

    }

    /**
     * @Route("owner_space", name="owner")
     */
    public function owner()
    {
        return $this->render('owner/owner_space.html.twig');
    }


}