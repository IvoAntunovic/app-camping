<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Bill;
use App\Repository\BillRepository;


/**
 * Class AdminController
 * @package App\Controller
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    


    public function __construct()
    {

    }





    /**
     * @Route("admin_space", name="admin")
     */
    public function admin()
    {
        return $this->render("admin/admin_space.html.twig");
    }





    /**
     * @Route("bills", name="billsAdmin")
     */
    public function bills(BillRepository $billRepository)
    {

          $user = $this->getUser();
          $id = $user->getId();
        
      $bills = $billRepository->findByforeignKey($id);

      $total = 0;

      foreach($bills as $bill){
        
          $lines = $bill->getBillLines();

          foreach($lines as $line){
                 $total += $line->getTotalPrice();
          }
        
      }

      $TVA = ($total / 100) * 20;

      $totalHT = $total - $TVA;

        return $this->render("admin/bills_list.html.twig", [
            'bills' => $bills,
            'total' => $total,
            'totalHT' => $totalHT
        ]);
    }


}