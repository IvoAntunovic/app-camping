<?php

namespace App\Controller;

use App\Entity\Property;
use App\Entity\BillLine;
use App\Entity\Bill;
use App\Entity\Address;
use App\Entity\Customer;
use App\Entity\Booking;
use App\Form\FormBookingType;
use App\AppBundle\BillLineHelper;
use App\Entity\BillCustomerInfo;
use App\Repository\BookingRepository;
use App\Repository\PropertyRepository;
use App\Repository\PropertyTypeRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



/**
 * Class HomeController
 * @package App\Controller
 * @Route("/home")
 */
class HomeController extends AbstractController
{



    public function __construct()
    {

    }

    



     /**
     * @Route("/{message}", name="home", methods={"GET"})
     *
     * 
     */
    public function home(string $message = '' ,PropertyTypeRepository $propertyTypeRepository)
    {

       $types =  $propertyTypeRepository->findAll();

        return $this->render("home/home.html.twig", [
            'types' => $types,
            'message' => $message
        ]);

    }







    /**
     * @Route("/detail/{id}", name="detail", methods={"GET", "POST"})
     */
    public function detail($id, PropertyTypeRepository $propertyTypeRepository,
                           PropertyRepository $propertyRepository,
                           Request $request){
   
        $type = $propertyTypeRepository->find($id);  
        $property = $propertyRepository->findOneBySomeforeignKey($id);

        $manager = $this->getDoctrine()->getManager();

        $booking = new Booking();
        $customer = new Customer();
        $address = new Address();
        
        
        $form = $this->createForm(FormBookingType::class); 
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
            
            
            
            // Création d'une adresse
            $address->setCity($_POST['form_booking']['city'])
            ->setStreet($_POST['form_booking']['street']);
            $manager->persist($address);
           
           
           // Création d'un client
           $customer->setCustomerFirstName($_POST['form_booking']['firstName'])
           ->setCustomerName($_POST['form_booking']['name'])
           ->setEmail($_POST['form_booking']['email'])
           ->setPhonNumber($_POST['form_booking']['phonNumber'])
           ->setAddress($address);
           $manager->persist($customer);
           

                 
                 
                // Stockage des date de début en forma Années/Mois/Jours 
                $month = $_POST['form_booking']['startDate']["month"];
                $day = $_POST['form_booking']['startDate']["day"];
                $year = $_POST['form_booking']['startDate']["year"];

                // Stockage de la date de fin en forma Années/Mois/Jours
                $end_month = $_POST['form_booking']['endDate']["month"];
                $end_day = $_POST['form_booking']['endDate']["day"];
                $end_year = $_POST['form_booking']['endDate']["year"];


                // Construction de la date avec séparateur "-" format OBJET
                 $startObject = new DateTime($year . '-' . $month . '-' . $day);
                 $endObject = new DateTime($end_year . '-' . $end_month . '-' . $end_day);
                
                 // Construction de la date avec séparateur "-" format STRING
                 $startString = $year . '-' . $month . '-' . $day;
                 $endString = $end_year . '-' . $end_month . '-' . $end_day;
                 


                 // Création de la facture en trois parties 
                 // FACTURE PARTIE 1 Création de l'identifiant de la facture et de sa date de création
                 $bill = new Bill();
                 $bill->setCreatedAt(new DateTime())
                      ->setCompanyName("Camping de l'espadrille volante")
                      ->setOwnerNumber($property[0]->getUser()->getId());
                      
                 $manager->persist($bill);
                 


                 // Création d'une réservation
                 $booking->setProperty($property[0])
                 ->setStartDate($startObject)
                 ->setEndDate($endObject)
                 ->setSleepingAdult($_POST['form_booking']['sleepingAdult'])
                 ->setSleepingChild($_POST['form_booking']['sleepingChild'])
                 ->setCustomer($customer)
                 ->setSwimingPoolOption($_POST['form_booking']['swimingPoolOption']);

                 $swimingAdultPrice = 1.5;
                 $swimingChildPrice = 1;

                

                 if($_POST['form_booking']['swimingPoolOption'] == 1){


                    if($_POST['form_booking']['swimingPoolAdult'] < 1){
                        $booking->setSwimingPoolAdult(1);
                        $billLineSwimingAdult = BillLineHelper::lineHelper($bill, "Tiecket piscine pour adulte", $booking->getSwimingPoolAdult(), $swimingAdultPrice ,$swimingAdultPrice * $booking->getSwimingPoolAdult());
                        $manager->persist($billLineSwimingAdult);
                    }
                    else {
                        $booking->setSwimingPoolAdult($_POST['form_booking']['swimingPoolAdult']);
                        $billLineSwimingAdult = BillLineHelper::lineHelper($bill, "Tiecket piscine pour adulte", $booking->getSwimingPoolAdult(), $swimingAdultPrice ,$swimingAdultPrice * $booking->getSwimingPoolAdult());
                        $manager->persist($billLineSwimingAdult);
                    }
                     


                     if($_POST['form_booking']['sleepingChild'] > 0){
                        $booking->setSwimingPoolChild($_POST['form_booking']['swimingPoolChild']);
                        $billLineSwimingChild = BillLineHelper::lineHelper($bill, "Tickets piscine pour enfant", $booking->getSwimingPoolChild(), $swimingChildPrice ,$swimingChildPrice * $booking->getSwimingPoolChild());
                        $manager->persist($billLineSwimingChild);

                     }
                     else {
                        $booking->setSwimingPoolChild(0);
                        $booking->setSwimingPoolChild($_POST['form_booking']['swimingPoolChild']);
                        $billLineSwimingChild = BillLineHelper::lineHelper($bill, "Tickets piscine pour enfant", $booking->getSwimingPoolChild(), $swimingChildPrice ,$swimingChildPrice * $booking->getSwimingPoolChild());
                        $manager->persist($billLineSwimingChild);
                     }
                }
    

                 $manager->persist($booking);
                 
                 
                 
                // Conversion de la date de début et de la date de fin en nombre de seconde
                $start_sec_forma = strtotime($startString);
                 $end_sec_forma = strtotime($endString);
                 
                 // Nombre de secondes contenues dans un jour
                //  $day = 86400;
                 
                 // Formule simple pour calculer la durée du séjour en jours
                 $lengthOfStay  = ($end_sec_forma -= $start_sec_forma) / 86400;

                 
                  
                 

                 // FACTURE PARTIE 2 Création des lignes de facturation
                 $price = 0;
                 $childTaxe = 0.35;
                 $adultTaxe = 1.5;
                 

                 // Si il ya un ou plusieurs adulte on crée la ligne de facuration correspondant 
                 if($booking->getSleepingAdult() > 0){
                     // On multipli la taxe par le nombre total de personnes
                     $adultTaxeTotal =  1.5 * $booking->getSleepingAdult();
                     
                     $billLineAdult = BillLineHelper::lineHelper($bill, "Nombre d'adulte", $booking->getSleepingAdult(), $adultTaxe , $adultTaxeTotal);
                     $manager->persist($billLineAdult);

                    }
                    
                   // Si il y à un enfant ou plus on crée la ligne de facturation correspondant
                    if($booking->getSleepingChild() > 0){
                        
                        // On multipli la taxe enfant par le nombre d'enfant présent
                          $childTaxeTotal = 0.35 * $booking->getSleepingChild();

                          $billLineChild = BillLineHelper::lineHelper($bill, "Nombre d'enfant", $booking->getSleepingAdult(), $childTaxe , $childTaxeTotal);

                          $manager->persist($billLineChild);
                    }
                 
                    

                 // On teste le type de location ainsi que la taille et on assigne à la variable "$prix" le tarif corrspondant
                 if($type->getLabel() == 'Mobil home'){

                    if ($type->getSleepingOrSurface() == 3) {  

                        $price = 20;
                    }
                    if($type->getSleepingOrSurface() == 4){
                        $price = 24;
                    }
                    if($type->getSleepingOrSurface() == 5) {
                        $price = 27;
                    }
                    if($type->getSleepingOrSurface() == 8){
                        $price = 34;
                    }

                    // Une fois qu'on connait le type de location exacte ainsi que sont tarif, on crée la ligne de facturation liée à la propriété
                    $billLineProperty = BillLineHelper::lineHelper($bill, $type->getLabel() . ' ' . $type->getSleepingOrSurface() . ' places', 1, $price, $price); 
                    $manager->persist($billLineProperty);

                                     
                 // Maintenant qu'on connait le prix exacte de la location, on le multipli par le nombres de jours réservés
                 // Pour pouvoir créer la ligne de facturation du prix du sejour                 
                 $billLineLengthOfStay = BillLineHelper::lineHelper($bill, "Nombre de jours", $lengthOfStay, $price, $price * $lengthOfStay);
                 $manager->persist($billLineLengthOfStay);

                 } 


                 if($type->getLabel() == 'Caravane'){

                    if($type->getSleepingOrSurface() == 2){
                        $price = 15;
                    }
                    if($type->getSleepingOrSurface() == 4){
                        $price = 18;
                    }
                    if($type->getSleepingOrSurface() == 6){
                        $price = 24;
                    }

                    
                    $billLineProperty = BillLineHelper::lineHelper($bill, $type->getLabel() . ' ' . $type->getSleepingOrSurface() . ' places', 1, $price, $price); 
                    $manager->persist($billLineProperty);
 
                 $billLineLengthOfStay = BillLineHelper::lineHelper($bill, "Nombre de jours", $lengthOfStay, $price, $price * $lengthOfStay);
                 $manager->persist($billLineLengthOfStay);
  
                 }


                 if($type->getLabel() == 'Terrain'){

                     if($type->getSleepingOrSurface() == 8) {
                        $price = 12;
                    }
                    if($type->getSleepingOrSurface() == 14){
                        $price = 14;
                    }
                     
                    $billLineProperty = BillLineHelper::lineHelper($bill, $type->getLabel() . ' ' . $type->getSleepingOrSurface() . ' m2', 1, $price, $price); 
                    $manager->persist($billLineProperty);

                 $billLineLengthOfStay = BillLineHelper::lineHelper($bill, "Nombre de jours", $lengthOfStay, $price, $price * $lengthOfStay);
                 $manager->persist($billLineLengthOfStay);

                 }





                 // FACTURE PARTIE 3 Création de la partie client de la facture
                 $billCustomerInfo = new BillCustomerInfo();
                 $billCustomerInfo->setBill($bill)
                                  ->setFirstName($customer->getCustomerFirstName())
                                  ->setName($customer->getCustomerName())
                                  ->setCity($address->getCity())
                                  ->setStreet($address->getStreet());

                                  $manager->persist($billCustomerInfo);
                                  


                //  dump($request->request->get("form_booking")["startDate"] );
                 $manager->flush(); 

                    $message = "Merci d'avoir réservé l'une de nos locations, à très bientot ☻";

                    return $this->redirectToRoute('home', [                        
                        'message' => $message
                    ]);
                }

                
                return $this->render("home/detail.html.twig", [
                    'type' => $type,
                    'property' => $property,
                    'form' => $form->createView(),
        ]);
    }





    /**
     *@Route("/billCustomer/{billLines}", name="billCustomer")
     */
    public function billCustomer($billLines)
    {
        return $this->render("home/bill_customer.html.twig", [
             'billLines' => $billLines
        ]);
    }

    
}