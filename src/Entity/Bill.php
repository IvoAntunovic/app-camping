<?php

namespace App\Entity;

use App\Repository\BillRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BillRepository::class)
 */
class Bill
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity=BillCustomerInfo::class, mappedBy="bill", cascade={"persist", "remove"})
     */
    private $billCustomerInfo;

    /**
     * @ORM\OneToMany(targetEntity=BillLine::class, mappedBy="bill")
     */
    private $billLines;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $companyName;

    /**
     * @ORM\Column(type="integer")
     */
    private $ownerNumber;

    

    public function __construct()
    {
        $this->billLines = new ArrayCollection();
    }

 

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getBillCustomerInfo(): ?BillCustomerInfo
    {
        return $this->billCustomerInfo;
    }

    public function setBillCustomerInfo(BillCustomerInfo $billCustomerInfo): self
    {
        // set the owning side of the relation if necessary
        if ($billCustomerInfo->getBill() !== $this) {
            $billCustomerInfo->setBill($this);
        }

        $this->billCustomerInfo = $billCustomerInfo;

        return $this;
    }

    /**
     * @return Collection|BillLine[]
     */
    public function getBillLines(): Collection
    {
        return $this->billLines;
    }

    public function addBillLine(BillLine $billLine): self
    {
        if (!$this->billLines->contains($billLine)) {
            $this->billLines[] = $billLine;
            $billLine->setBill($this);
        }

        return $this;
    }

    public function removeBillLine(BillLine $billLine): self
    {
        if ($this->billLines->removeElement($billLine)) {
            // set the owning side to null (unless already changed)
            if ($billLine->getBill() === $this) {
                $billLine->setBill(null);
            }
        }

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getOwnerNumber(): ?int
    {
        return $this->ownerNumber;
    }

    public function setOwnerNumber(int $ownerNumber): self
    {
        $this->ownerNumber = $ownerNumber;

        return $this;
    }




}
