<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $property;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;



    /**
     * @ORM\Column(type="integer")
     */
    private $sleepingAdult;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sleepingChild;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $swimingPoolOption;

    /**
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @ORM\Column(type="date")
     */
    private $endDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $swimingPoolAdult;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $swimingPoolChild;

   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

  

  

  

    public function getSleepingAdult(): ?int
    {
        return $this->sleepingAdult;
    }

    public function setSleepingAdult(int $sleepingAdult): self
    {
        $this->sleepingAdult = $sleepingAdult;

        return $this;
    }

    public function getSleepingChild(): ?int
    {
        return $this->sleepingChild;
    }

    public function setSleepingChild(?int $sleepingChild): self
    {
        $this->sleepingChild = $sleepingChild;

        return $this;
    }

    public function getSwimingPoolOption(): ?int
    {
        return $this->swimingPoolOption;
    }

    public function setSwimingPoolOption(?int $swimingPoolOption): self
    {
        $this->swimingPoolOption = $swimingPoolOption;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getSwimingPoolAdult(): ?int
    {
        return $this->swimingPoolAdult;
    }

    public function setSwimingPoolAdult(?int $swimingPoolAdult): self
    {
        $this->swimingPoolAdult = $swimingPoolAdult;

        return $this;
    }

    public function getSwimingPoolChild(): ?int
    {
        return $this->swimingPoolChild;
    }

    public function setSwimingPoolChild(?int $swimingPoolChild): self
    {
        $this->swimingPoolChild = $swimingPoolChild;

        return $this;
    }

  
}
