<?php

namespace App\Entity;

use App\Repository\PropertyTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;



/**
 * @ORM\Entity(repositoryClass=PropertyTypeRepository::class)
 * @Vich\Uploadable()
 */
class PropertyType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;


    
    /**
     * @ORM\OneToMany(targetEntity=Property::class, mappedBy="propertyType")
     */
    private $properties;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $file;

    /**
     * @var File\null
     * @Vich\UploadableField(mapping="camping_images", fileNameProperty="file")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="integer")
     */
    private $sleepingOrSurface;

    

    public function __construct()
    {
        $this->properties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

 

    /**
     * @return Collection|Property[]
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    public function addProperty(Property $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
            $property->setPropertyType($this);
        }

        return $this;
    }

    public function removeProperty(Property $property): self
    {
        if ($this->properties->removeElement($property)) {
            // set the owning side to null (unless already changed)
            if ($property->getPropertyType() === $this) {
                $property->setPropertyType(null);
            }
        }

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function setImageFile(File $file = null)
    {


        if ($file) {

            $this->cratedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getSleepingOrSurface(): ?int
    {
        return $this->sleepingOrSurface;
    }

    public function setSleepingOrSurface(int $sleepingOrSurface): self
    {
        $this->sleepingOrSurface = $sleepingOrSurface;

        return $this;
    }

  

 
}
