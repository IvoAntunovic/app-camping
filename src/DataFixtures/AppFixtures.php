<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\User;
use App\Entity\PropertyType;
use App\Entity\Address;
use App\Entity\Booking;
use App\Entity\Customer;
use App\Entity\Property;
use Faker;

class AppFixtures extends Fixture
{


 /**
  * @var UserPasswordHasherInterface
  */
   private $encoder;

  public function __construct(UserPasswordHasherInterface $encoder) {
      $this->encoder = $encoder;
  }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);


        $faker = Faker\Factory::create('fr_FR');

    

        // Création de deux utilisateurs   
               for($i = 0; $i < 10; $i++){
                   
                   
                   if($i == 1){
                       
                    $u = new User();
                    $u->setEmail("ivo@orange.fr")
                    ->setPassword($this->encoder->hashPassword($u, 'ivo'))
                    ->setRoles(["ROLE_ADMIN"]);

                    $this->setReference('U' . $i, $u);
                    $manager->persist($u);


                 }
                 if($i == 2){

                   $u = new User();
                   $u->setEmail("iva@orange.fr")
                   ->setPassword($this->encoder->hashPassword($u, 'ivo'))
                   ->setRoles(["ROLE_OWNER"]);

                   $this->setReference('U' . $i, $u);
                   $manager->persist($u);
                 }
                       
                 
    }



    // Création de trois types de propriété

       $i = 0;
       $types = ['Mobil home', 'Caravane', 'Terrain'];

       foreach ($types as $key => $type){

           $i++;

           $t = new PropertyType();

           $t->setLabel($type);


           if($i == 1){
               $t->setFile('mobil-home.jpg');
               $t->setSleepingOrSurface(8);
               
           }    
           if($i == 2){
               $t->setFile('caravane.jpg');
               $t->setSleepingOrSurface(6);
            }
            if($i == 3){
                $t->setFile('terrain.png');
                $t->setSleepingOrSurface(14);
            }

           $this->setReference('T'. $i, $t);
           $manager->persist($t);
       }


       





       // Création de plusieurs adresses
       $cities = [
                    'TARTAROS SUR MERE',
                    'ÉCOUANT','LE SOLER', 
                    'FORT FORT LOINTAIN',
                    'forêt', 'nuguets', 
                    'Ille sur Têt', 'DYSNEYLAND',
                    'PERPIGNANGAN', 'CHOCOLAND'
                 ];

        $streets = [
                      '1 rue leon blum', '78 Avenue Pasteur',
                      '3000 rue des menteurs', '00 Avenue TOTO',
                      'Avenue Avenue', '21 JumpStreet', '5 rue Street fighter'
                   ];

        foreach($cities as $key => $city){

            $a = new Address();

            $a->setCity($city);
      
            $a->setStreet( $streets[rand(0, count($streets) - 1 )]);

            $this->setReference('A' . $key, $a);

            $manager->persist($a);
        }

        


     // Création de plusieurs clients
     for($u = 0; $u < 10; $u++) {

               $c = new Customer();
               $c->setAddress($this->getReference('A'. rand(0, 9) ))
                 ->setCustomerFirstName($faker->firstname)
                 ->setCustomerName($faker->name)
                 ->setEmail($faker->email)
                 ->setPhonNumber($faker->phoneNumber);
                 
                 $this->setReference('C'. $u, $c);
                 $manager->persist($c);
       }
           





      // Création de plusieurs propriétés pour le camping 
      for($count = 0; $count < 90; $count++){

           

        if($count < 30) {
            $p = new Property(); 
            $p->setUser($this->getReference('U'. 2));
            $p->setPropertyType($this->getReference('T'. 1));
            $p->setPrice(34);
            $this->setReference('P'. $count, $p);
            $manager->persist($p); 
        }

        if($count > 30) {
            $p = new Property();
            $p->setUser($this->getReference('U'. 1));
            $p->setPropertyType($this->getReference('T'. 1));
            $p->setPrice(34);

            $this->setReference('P'. $count, $p);
            $manager->persist($p); 
        }
        if($count > 50) {
            $p = new Property();
            $p->setUser($this->getReference('U'. 1));
            $p->setPropertyType($this->getReference('T'. 2));
            $p->setPrice(24);   

            $this->setReference('P'. $count, $p);
            $manager->persist($p);      
        }
        if($count > 60) {
            $p = new Property();
            $p->setUser($this->getReference('U'. 1));
            $p->setPropertyType($this->getReference('T'. 3));
            $p->setPrice(14);

            $this->setReference('P'. $count, $p);
            $manager->persist($p); 
        }
             
    }




    
    // Création d'une réservation
     for($b_count = 0; $b_count < 10; $b_count++){


         $b = new Booking();
         $b->setProperty($this->getReference('P'. rand(1, 9)))
           ->setCustomer($this->getReference('C'. rand(1, 9)))
           ->setStartDate($faker->datetime)
           ->setEndDate($faker->datetime)
           ->setSleepingAdult(5)
           ->setSleepingChild(2)
           ->setSwimingPoolOption(1);

           $manager->persist($b);
     }

            
            
            $manager->flush();


      }
}